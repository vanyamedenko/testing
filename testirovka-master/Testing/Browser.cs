﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using System.IO;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;


namespace Testing
{
    public static class Browser
    {
        public static ChromeDriver WebDriver { get; private set; } = null;
        public static void SetupChromeWebDriver()
        {
            WebDriver = new ChromeDriver(@"D:\testirovka-master\chromedriver_win32");
        }

        public static IWebDriver getInstance() {
            return WebDriver;
        }

        public static void Quit()
        {
            if (WebDriver != null)
            {
                WebDriver.Quit();
            }
        }
    }
}
