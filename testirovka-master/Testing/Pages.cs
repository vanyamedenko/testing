﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.PageObjects;

namespace Testing
{
    public static class Pages
    {
        private static T getPages<T>() where T : new()
        {
            var page = new T();
            PageFactory.InitElements(Browser.WebDriver, page);
            return page;
        }

        public static PageObjects.MainPage MainPage => getPages<PageObjects.MainPage>();

        public static PageObjects.AccidentsPage AccidentsPage => getPages<PageObjects.AccidentsPage>();
    }
}
