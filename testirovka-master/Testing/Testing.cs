﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using Faker;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;
using Allure.Commons;

namespace Testing
{
    [AllureNUnit]
    [TestFixture]
    public class Testing
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            Browser.SetupChromeWebDriver();
            driver = Browser.getInstance();
            baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }


        [Test]
        [AllureStory]
        [AllureTag("NUnit")]
        [Category("News"), Description("Main page")]
        public void SearchTest()
        {
            string searchTerm = Faker.TextFaker.Sentence().Split(" ")[0];
            var page = Pages.RedditPage;
            page.GoToUrl();
            page.SearchBar.SendKeys(searchTerm);
            page.SearchBar.SendKeys(Keys.Enter);
            Assert.AreEqual(searchTerm, page.getSearchQueryParam());
        }

        [Test]
        [AllureStory]
        [AllureTag("NUnit")]
        [Category("News"), Description("Monitoring")]
        public void SignUpTest_1()
        {
            string email = Faker.InternetFaker.Email();
            var page = Pages.SignupPage;
            page.GoToUrl();
            page.emailInput.SendKeys(email);
            ((ITakesScreenshot)driver).GetScreenshot().SaveAsFile(@"D:\testirovka-master\screenshot\test.png", ScreenshotImageFormat.Png);
            page.nextButton.Click();
            Assert.IsTrue(page.isOnSecondPage());
        }
    }
}
