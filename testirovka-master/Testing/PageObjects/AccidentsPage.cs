﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing.PageObjects
{
    public class AccidentsPage : Page
    {
        public AccidentsPage() { }
        public override string Url => "https://fakty.com.ua/ru/proisshestvija/";

        
        [FindsBy(How = How.ClassName, Using = "blogs_news blogs_news_category orange_line")]
        public IWebElement checkNews { get; }

        [FindsBy(How = How.CssSelector, Using = ".orange_line")]
        public IWebElement nextPage { get; }

        public bool isOnSecondPage()
        {
            return driver.FindElement(By.XPath("//div[.='Check news']")).Displayed;
        }
    }
}
