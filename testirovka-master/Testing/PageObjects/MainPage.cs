﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using System.IO;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;


namespace Testing.PageObjects
{
    public class MainPage : Page
    {
        public MainPage() { }
        public override string Url => "https://fakty.com.ua/ru/";

        public void showPage()
        {
            driver.FindElement(By.ClassName("logo")).Click();
        }

        public void clicSearch()
        {
            driver.FindElement(By.ClassName("icon-search icon-search-header")).Click();
        }
       
    }
}
